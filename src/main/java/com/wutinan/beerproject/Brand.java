/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wutinan.beerproject;

/**
 *
 * @author INGK
 */
public class Brand {

    public static void main(String[] args) {
        Beer Leo_320 = new Beer("Leo 320 ml.", 35);
        Beer Leo_can_490 = new Beer("LeoCan 490 ml.", 46);
        Beer Leo_620 = new Beer("Leo 620 ml.", 55);
        Beer SingHa_320 = new Beer("SingHa 320 ml.", 39);
        Beer SingHa_can_320 = new Beer("SingHaCan 320 ml.", 35);
        Beer SingHa_620 = new Beer("SingHa 620 ml.", 59);
        Beer Heineken_can_320 = new Beer("HeinekeCan 320 ml.", 34);

        System.out.println(Leo_320.name + "  total = " + Leo_320.amount);
        System.out.println(Leo_can_490.name + "  total = " + Leo_can_490.amount);
        System.out.println(Leo_620.name + "  total = " + Leo_620.amount);
        System.out.println(SingHa_320.name + "  total = " + SingHa_320.amount);
        System.out.println(SingHa_can_320.name + "  total = " + SingHa_can_320.amount);
        System.out.println(SingHa_620.name + "  total = " + SingHa_620.amount);
        System.out.println(Heineken_can_320.name + "  total = " + Heineken_can_320.amount);

    }

}
